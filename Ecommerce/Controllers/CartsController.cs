﻿using Ecommerce.Models;
using Ecommerce.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Ecommerce.Controllers
{
    [RoutePrefix("Cart")]
    public class CartsController : ApiController
    {
        private ICartRepository cartRepository;
        public CartsController(ICartRepository cartRepository)
        {
            this.cartRepository = cartRepository;
        }

        [HttpGet]
        [Route("GetCarts")]
        public IEnumerable<Object> GetCarts()
        {
            return cartRepository.GetCarts();
        }

        [HttpGet]
        [Route("GetCartById/{id}")]
        public Object GetCartById([FromUri] int id)
        {
            return cartRepository.GetCartById(id);
        }

        [HttpPost]
        [Route("PostCart")]
        public HttpResponseMessage PostCart([FromBody]CartDto obj)
        {
            try
            {
                cartRepository.PostCart(obj);
                var msg = Request.CreateResponse(HttpStatusCode.Created, obj);
                msg.Headers.Location = new Uri(Request.RequestUri
                    + obj.cartId.ToString());
                return msg;
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }


        [Route("PutCart/{id}")]
        public IHttpActionResult PutCart(int id, CartDto cart)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cart.cartId)
            {
                return BadRequest();
            }

            try
            {
                cartRepository.PutCart(id, cart);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CartExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }



        [HttpDelete]
        [Route("DeleteCart/{id}")]
        public HttpResponseMessage DeleteCart(int id)
        {
            bool status = cartRepository.DeleteCart(id);
            if (status)
            {
                var msg = Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                return msg;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Requested Data Not found");
            }
        }


        private bool CartExists(int id)
        {
            return cartRepository.CartExists(id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataAceessLayer;
using Ecommerce.Repository;
using System.Data.Entity.Infrastructure;
using Ecommerce.Models;

namespace Ecommerce.Controllers
{
    [RoutePrefix("Category")]
    public class CategoriesController : ApiController
    {
        private ICategoryRepository categoryRepository;
       
        /*public CategoriesController()
        {
            this.categoryRepository = new CategoryRepository(new EcommerceEntities());
        }*/

        public CategoriesController(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        [HttpGet]
        [Route("GetCategories")]
        public IEnumerable<Object> GetCategories()
        {
            return categoryRepository.GetCategories();
        }

        [HttpGet]
        [Route("GetCategoryById/{id}")]
        public Object GetCategoryById([FromUri] int id)
        {
             return categoryRepository.GetCategoryById(id);
        }

        [HttpPost]
        [Route("PostCategory")]
        public HttpResponseMessage PostCategory([FromBody]CategoryDto obj)
        {
            try
            {
                categoryRepository.PostCategory(obj);
                var msg = Request.CreateResponse(HttpStatusCode.Created, obj);
                msg.Headers.Location = new Uri(Request.RequestUri
                    + obj.categoryId.ToString());
                return msg;
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }

      
        [Route("PutCategory/{id}")]
        public IHttpActionResult PutCategory(int id, CategoryDto category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != category.categoryId)
            {
                return BadRequest();
            }

            try
            {
                categoryRepository.PutCategory(id, category);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        
      
        [HttpDelete]
        [Route("DeleteCategory/{id}")]
        public IHttpActionResult DeleteCategory(int id)
        {
            bool status = categoryRepository.DeleteCategory(id);
            if (status)
            {
                return StatusCode(HttpStatusCode.OK);
            }
            else
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                categoryRepository.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return categoryRepository.CategoryExists(id);
        }
    }
}

﻿using DataAceessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace Ecommerce.Controllers
{
    [RoutePrefix("Login")]
    public class LoginController : ApiController
    {
        EcommerceEntities db = new EcommerceEntities();



        [Route("UserLogin")]
        [HttpPost]
        public HttpResponseMessage UserLogin([FromBody]User user)
        {
            User c = db.Users.Where(x => x.name == user.name).FirstOrDefault();

            if (c == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "The user was not found.");
            byte[] SrctArray;
            string key = "1prt56";
            byte[] EnctArray = UTF8Encoding.UTF8.GetBytes(user.password);

            SrctArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider objt = new TripleDESCryptoServiceProvider();

            MD5CryptoServiceProvider objcrpt = new MD5CryptoServiceProvider();

            SrctArray = objcrpt.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));

            objcrpt.Clear();

            objt.Key = SrctArray;

            objt.Mode = CipherMode.ECB;

            objt.Padding = PaddingMode.PKCS7;

            ICryptoTransform crptotrns = objt.CreateEncryptor();

            byte[] resArray = crptotrns.TransformFinalBlock(EnctArray, 0, EnctArray.Length);

            objt.Clear();

            user.password = Convert.ToBase64String(resArray, 0, resArray.Length);

            bool credentials = db.Users.Any(x => x.name == user.name && x.password == user.password);

            if (!credentials) return Request.CreateResponse(HttpStatusCode.Forbidden,
                "The username/password combination was wrong.");

            return Request.CreateResponse(HttpStatusCode.OK, TokenManager.GenerateToken(c.name));

        }

        //[Route("Validate")]
        //[HttpGet]
        //public HttpResponseMessage Validate(string token, string username)
        //{
        //    int UserId = new UserRepository().GetUser(username);
        //    if (UserId == 0) return new ResponseVM
        //    {
        //        Status = "Invalid",
        //        Message = "Invalid User."
        //    };
        //    string tokenUsername = TokenManager.ValidateToken(token);
        //    if (username.Equals(tokenUsername))
        //    {
        //        return new ResponseVM
        //        {
        //            Status = "Success",
        //            Message = "OK",
        //        };
        //    }
        //    return new ResponseVM
        //    {
        //        Status = "Invalid",
        //        Message = "Invalid Token."
        //    };
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataAceessLayer;
using Ecommerce.Repository;
using System.Web.Http.Description;
using System.Data.Entity.Infrastructure;
using Ecommerce.Models;

namespace Ecommerce.Controllers
{
    [RoutePrefix("Order")]
    public class OrdersController : ApiController
    {
        private IOrderRepository orderRepository;
        /*public OrdersController()
        {
            this.orderRepository = new OrderRepository(new EcommerceEntities());
        }*/
        public OrdersController(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }
        [HttpGet]
        [Route("GetOrders")]
        public IEnumerable<Object> GetOrders()
        {

            return orderRepository.GetOrders();
        }

        [HttpGet]
        [Route("GetOrderByUid/{uid}")]
        public IEnumerable<Object> GetOrderByUid([FromUri] int uid)
        {

            return orderRepository.GetOrderByUid(uid);
        }

        [HttpGet]
        [Route("GetRecentOrders")]
        public IEnumerable<Object> GetRecentOrders()
        {

            return orderRepository.GetRecentOrders();
        }

        [HttpPost]
        [Route("PostOrder")]
        public HttpResponseMessage PostOrder([FromBody]OrderDto obj)
        {
            try
            {
                orderRepository.PostOrder(obj);
                var msg = Request.CreateResponse(HttpStatusCode.Created, obj);
                msg.Headers.Location = new Uri(Request.RequestUri
                    + obj.oid.ToString());
                return msg;
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }


        [Route("PutOrder/{id}")]
        public IHttpActionResult PutOrder(int id, OrderDto order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != order.oid)
            {
                return BadRequest();
            }

            
            try
            {
                orderRepository.PutOrder(id, order);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        [HttpDelete]
        [Route("DeleteOrder/{id}")]
        public HttpResponseMessage DeleteOrder(int id)
        {
            bool status = orderRepository.DeleteOrder(id);
            if (status)
            {
                var msg = Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                return msg;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Requested Data Not found");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                orderRepository.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            return orderRepository.OrderExists(id);
        }
    }
}

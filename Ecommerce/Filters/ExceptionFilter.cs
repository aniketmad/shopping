﻿namespace Ecommerce.Filters
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Filters;

    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            HttpStatusCode status = HttpStatusCode.InternalServerError;
            String message = String.Empty;
            var exceptionType = context.Exception.GetType();
            if (exceptionType == typeof(DivideByZeroException))
            {
                message = "Divide By Zero Exception.";
                status = HttpStatusCode.NotAcceptable;
            }
            else if (exceptionType == typeof(NotImplementedException))
            {
                message = "Method not implemented.";
                status = HttpStatusCode.NotImplemented; 
            }
            else if (exceptionType == typeof(NullReferenceException))
            {
                message = "Object Not Created.";
                status = HttpStatusCode.NotAcceptable;
            }
            else
            {
                message = "Not found.";
                status = HttpStatusCode.NotFound;
            }

            context.Response = new HttpResponseMessage()
            {
                Content = new StringContent(message, System.Text.Encoding.UTF8, "text/plain"),
                StatusCode = status
            };



        }
    }
}
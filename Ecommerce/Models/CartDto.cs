﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Models
{
    public class CartDto
    {
        public int cartId { get; set; }

        public int productId { get; set; }

        public int quantity { get; set; }

        public int userId { get; set; }

        public string productName { get; set; }

        public int price { get; set; }

    }
}
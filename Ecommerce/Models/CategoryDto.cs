﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Models
{
    public class CategoryDto
    {
        public int categoryId { get; set; }
        public string categoryName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Models
{
    public class OrderDto
    {
        public int oid { get; set; }

        public DateTime orderDate { get; set; }

        public int totalPrice { get; set; }

        public int deliverCharges { get; set; }

        public int userId { get; set; }
    }
}
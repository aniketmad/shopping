﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Models
{
    public class ProductDto
    {
        public int productId { get; set; }

        public string productName { get; set; }

        public int price { get; set; }

        public int quantity { get; set; }

        public string description { get; set; }

        public int categoryId { get; set; }

        public int cartId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Models
{
    public class ProductOrderDto
    {
        public int prodOrdId { get; set; }

        public int orderId { get; set; }

        public int productId { get; set; }
    }
}
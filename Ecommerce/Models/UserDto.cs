﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Models
{
    public class UserDto
    {
        public int userId { get; set; }

        public string name { get; set; }

        public string address { get; set; }

        public string phoneNo { get; set; }

        public string email { get; set; }

        public string password { get; set; }

        public string gender { get; set; }
    }
}
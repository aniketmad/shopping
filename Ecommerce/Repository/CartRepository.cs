﻿using AutoMapper;
using DataAceessLayer;
using Ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Ecommerce.Repository
{
    public class CartRepository:ICartRepository
    {
        private EcommerceEntities db;
        public CartRepository(EcommerceEntities db)
        {
            this.db = db;
        }
        public IList<CartDto> GetCarts()
        {
            var query = db.Carts;
            return query.ToList().Select(cat => Mapper.Map<Cart, CartDto>(cat)).ToList();
        }

        public CartDto GetCartById(int id)
        {
            var query = db.Carts.Where(c => c.cartId == id).FirstOrDefault();
            return Mapper.Map<Cart, CartDto>(query);
        }
        public void PutCart(int id, CartDto obj1)
        {
            Cart cart = Mapper.Map<CartDto, Cart>(obj1);
            db.Entry(cart).State = EntityState.Modified;
            db.SaveChanges();
        }
        public void PostCart(CartDto obj1)
        {
            Cart cart = Mapper.Map<CartDto, Cart>(obj1);
            db.Carts.Add(cart);
            db.SaveChanges();
        }
        public bool DeleteCart(int id)
        {
            bool status = false;
            Cart cart = db.Carts.Where(c => c.cartId == id).FirstOrDefault(); ;
            if (cart != null)
            {
                status = true;
                db.Carts.Remove(cart);
                db.SaveChanges();
                return status;
            }
            else
            {
                return status;
            }
        }
        public bool CartExists(int id)
        {
            return db.Carts.Count(e => e.cartId == id) > 0;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAceessLayer;
using System.Data.Entity;
using Ecommerce.Models;
using AutoMapper;

namespace Ecommerce.Repository
{
    public class CategoryRepository:ICategoryRepository,IDisposable
    {
        private EcommerceEntities db;
       
        public CategoryRepository(EcommerceEntities db)
        {
            this.db = db;

        }

        //public IQueryable<CategoryDto> GetCategories()
        //{
        //    return (from c in db.Categories
        //            select new CategoryDto
        //            {
        //                categoryId=c.categoryId,
        //                categoryName= c.categoryName
        //            }
        //        );
        //}

        //for mapping 
        public IList<CategoryDto> GetCategories()
        {  
            var query = db.Categories;
            return query.ToList().Select(cat => Mapper.Map<Category, CategoryDto>(cat)).ToList();
        }

       /* public CategoryDto GetCategoryById(int id)
        {
            var query = (from c in db.Categories
                         where c.categoryId == id
                         select new CategoryDto
                         {
                             categoryId = c.categoryId,
                             categoryName = c.categoryName
                         }).FirstOrDefault();
            return query;
        }*/

          //mapper
        public CategoryDto GetCategoryById(int id)
        {
            var query = (from c in db.Categories
                         where c.categoryId == id
                         select c).FirstOrDefault();
            return Mapper.Map<Category, CategoryDto>(query);
        }

        public void PutCategory(int id, CategoryDto obj)
        {
            Category category = Mapper.Map<CategoryDto, Category>(obj);
            db.Entry(category).State = EntityState.Modified;
            db.SaveChanges();
        }
        public void PostCategory(CategoryDto obj)
        {
            Category category = Mapper.Map<CategoryDto, Category>(obj);
            db.Categories.Add(category);
            db.SaveChanges();
        }
        public bool DeleteCategory(int id)
        {
            Category category = db.Categories.Where(c => c.categoryId == id).FirstOrDefault();
            bool status = false;
            if (category != null)
            {
                status = true;
                db.Categories.Remove(category);
                db.SaveChanges();
                return status;
            }
            else
            {
                //throw new HttpResponseException(HttpStatusCode.NotFound);
                return status;
            }
        }


        public bool CategoryExists(int id)
        {
            return db.Categories.Count(e => e.categoryId == id) > 0;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
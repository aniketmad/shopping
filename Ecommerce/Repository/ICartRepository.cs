﻿using Ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Repository
{
    public interface ICartRepository:IDisposable
    {
        IList<CartDto> GetCarts();
        CartDto GetCartById(int id);
        void PutCart(int id, CartDto cart);
        void PostCart(CartDto obj);
        bool DeleteCart(int id);
        bool CartExists(int id);
    }
}

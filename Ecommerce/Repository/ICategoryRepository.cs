﻿using DataAceessLayer;
using Ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Repository
{
    public interface ICategoryRepository:IDisposable
    {
        //IQueryable<CategoryDto> GetCategories();
        //CategoryDto GetCategoryById(int id);

        IList<CategoryDto> GetCategories();
        CategoryDto GetCategoryById(int id);
        void PutCategory(int id, CategoryDto category);
        void PostCategory(CategoryDto obj);
        bool DeleteCategory(int id);
        bool CategoryExists(int id);
    }
}

﻿using DataAceessLayer;
using Ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Repository
{
    public interface IOrderRepository:IDisposable
    {

        //IQueryable<OrderDto> GetOrders();
        //IQueryable<OrderDto> GetOrderByUid(int id);
        //IQueryable<Order> GetRecentOrders();
        IList<OrderDto> GetRecentOrders();
        IList<OrderDto> GetOrderByUid(int id);
        IList<OrderDto> GetOrders();
        void PutOrder(int id, OrderDto order);
        void PostOrder(OrderDto obj);
        bool DeleteOrder(int id);
        bool OrderExists(int id);
    }
}

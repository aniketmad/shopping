﻿using Ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAceessLayer;
namespace Ecommerce.Repository
{
    public interface IProductRepository:IDisposable
    {
        IList<ProductDto> GetProducts();
        ProductDto GetProductById(int id);
        void PutProduct(int id, ProductDto product);
        void PostProduct(ProductDto obj);
        bool DeleteProduct(int id);
        bool ProductExists(int id);
        //for uri post method
        void PostData(Product obj);
    }
}
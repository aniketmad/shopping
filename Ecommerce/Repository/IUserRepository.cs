﻿using DataAceessLayer;
using Ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Repository
{
    public interface IUserRepository:IDisposable
    {
        IList<UserDto> GetUsers();
        UserDto GetUserById(int id);
        void PutUser(int id, UserDto user);
        void PostUser(UserDto obj);
        bool DeleteUser(int id);
        bool UserExists(int id);
    }
}

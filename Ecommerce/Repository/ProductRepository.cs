﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAceessLayer;
using System.Data.Entity;
using System.Web.Http;
using Ecommerce.Models;
using AutoMapper;

namespace Ecommerce.Repository
{
    public class ProductRepository:IProductRepository,IDisposable
    {
        private EcommerceEntities db;
        public ProductRepository(EcommerceEntities db)
        {
            this.db = db;
        }
        public IList<ProductDto> GetProducts()
        {
            var query = db.Products;
            return query.ToList().Select(cat => Mapper.Map<Product, ProductDto>(cat)).ToList();
        }
        public ProductDto GetProductById(int id)
        {
            var query = db.Products.Where(p => p.productId == id).FirstOrDefault();
            return Mapper.Map<Product, ProductDto>(query);
        }
        public void PutProduct(int id, ProductDto obj1)
        {
            Product product = Mapper.Map<ProductDto, Product>(obj1);
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();
        }


        public void PostProduct([FromBody]ProductDto obj1)
        {
            Product product = Mapper.Map<ProductDto, Product>(obj1);
            db.Products.Add(product);
            db.SaveChanges();
        }

        public void PostData(Product obj)
        {

            db.Products.Add(obj);
            db.SaveChanges();

        }
        public bool DeleteProduct(int id)
        {
            bool status = false;
            Product product = db.Products.Where(p => p.productId == id).FirstOrDefault();
            if (product != null)
            {
                status = true;
                db.Products.Remove(product);
                db.SaveChanges();
                return status;
            }
            else
            {
                return status;
            }

        }

        public bool ProductExists(int id)
        {
            return db.Products.Count(e => e.productId == id) > 0;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}